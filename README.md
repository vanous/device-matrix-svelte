# device-matrix-gadgetbridge

Gadgetbridge feature compatibility matrix source:

- url: https://pages.codeberg.org/vanous/gadgetbridge-device-matrix/
- source: https://codeberg.org/vanous/device-matrix-gadgetbridge
- JSON generated via Gadgetbridge, this branch/file: https://codeberg.org/vanous/Gadgetbridge/src/branch/CompatibilityMatrix/app/src/test/java/nodomain/freeyourgadget/gadgetbridge/test/DeviceMatrix.java
